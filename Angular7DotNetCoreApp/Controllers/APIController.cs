using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace Angular7DotNetCoreApp.Controllers
{
    //[Route("api/values/[controller]")]
    //[Route("api/[controller]")] works as https://localhost:5001/api/test
    //public class TestController : Controller

    [ApiController]
    public class TestController : ControllerBase
    {
        SQLReadOperations sql = new SQLReadOperations();

        [Route("api/values/[controller]")]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/SQL/Read")]
        //public ActionResult<IEnumerable<char>> ReadSQL()
        public ActionResult<List<string>> ReadSQL()
        {
            var result = sql.ReadColumn("FirstName");
            return result;

            //return new string[] { "sql1", "sq2" };
        }

        [Route("api/SQL/GetAllDataFromTable")]
        //public ActionResult<IEnumerable<char>> ReadSQL()
        public ActionResult<List<EmployeesClass>> GetRowsFromTable()
		{

			var result = sql.GetAllDataFromTable(sql.SQLTable);
            return result;

            //return new string[] { "sql1", "sq2" };
        }

        //public HttpResponseMessage Get()
        //{
        //    return new HttpResponseMessage()
        //    {
        //        Content = new StringContent("GET: Test message")
        //    };
        //}

        [HttpGet("[action]")] // URL: /api/TestControllerMethod
        public ActionResult<IEnumerable<string>> ModelTest()
        {
            return new string[] { "value1", "value2" };
        }

        private static string[] Summaries = new[]
{
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
