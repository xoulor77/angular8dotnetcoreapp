﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Angular7DotNetCoreApp
{
    public class SQLReadOperations : SQLConnection
    {
        public SqlCommand command;
        public SqlDataReader dataReader;
        public SqlDataAdapter adapter = new SqlDataAdapter();

        public List<string> ReadColumn(string columnName)
        {
            //ReadDataFromTable
            string query = "";
            //string output = "";
            var outputList = new List<string>();
            query = $"Select {columnName} from {SQLTable}";
            command = new SqlCommand(query, this.connection);
            dataReader = command.ExecuteReader();

            //if (dataReader.Read())
            if (dataReader.HasRows)
            {
                 foreach(var item in dataReader)
                {
                    var output = dataReader.GetValue(0).ToString();
                    outputList.Add(output);
                }

                
            }

            dataReader.Close();
            command.Dispose();
            //connection.Close();

            return outputList;
        }

        public List<EmployeesClass> GetAllDataFromTable(string sqlTable)
        {
            //ReadDataFromTable
            var resultList = new List<EmployeesClass>();
            string query = "";
            //string output = "";
            var outputList = new List<string>();
            query = $"Select {'*'} from {sqlTable}";
            command = new SqlCommand(query, this.connection);
            dataReader = command.ExecuteReader();

            //if (dataReader.Read())
            if (dataReader.HasRows)
            {
                //[todo]: try this https://stackoverflow.com/questions/21612005/stuck-on-sqldatareader-getvalues-method
                while(dataReader.Read())
                {
                    var tempResult = new EmployeesClass();
                    tempResult.EmployeeID = dataReader.GetInt32(0);
                    tempResult.LastName = dataReader.GetString(1);
                    tempResult.FirstName = dataReader.GetString(2);
                    tempResult.Address = dataReader.GetString(3);
                    tempResult.City = dataReader.GetString(4);
                    resultList.Add(tempResult);
                }
            }
            else
            {
                //throw exception for not having rows
                throw (new Exception("does not have rows in sepcified SQL table"));
            }

            dataReader.Close();
            command.Dispose();
            //connection.Close();

            return resultList;
        }

        public void GetAllData()
        {

        }

        public void QueryData()
        {

        }
    }
}
