﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Angular7DotNetCoreApp
{
    public class SQLConnection
    {
        public string SQLServer { get; set; } = "localhost";
        public string SQLDatabase { get; set; } = "SQLCRUD_TESTDB";
        public string SQLTable { get; set; } = "Employees";
        public SqlConnection connection { get; set; }

        public SQLConnection()
        {

            //try
            //{
            //    if (connection != null && connection.State == System.Data.ConnectionState.Closed)
            //    {
            ConnectToSQLServer();
            //    }
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
        }

        //public bool GetConnectionStatus(SqlConnection connection)
        //{
        //    bool isConnected;
        //    try
        //    {
        //        if (connection.State == System.Data.ConnectionState.Closed)
        //        {
        //            isConnected = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        isConnected = true;
        //    }

        //    return isConnected;
        //}

        public void ConnectToSQLServer()
        {
            string connectionString;
            //SqlConnection connection;
            //connetionString = @"Data Source=WIN-50GP30FGO75;Initial Catalog=Demodb;User ID=sa;Password=demol23";
            connectionString = $"Server={SQLServer};Database={SQLDatabase};Trusted_Connection=True;";
            connection = new SqlConnection(connectionString);
            connection.Open();
            Console.WriteLine("Successfully Connected to SQL Server: {0}", connection.Database);

        }

        //may not be needed
        public void CloseConnection()
        {
            if (connection.State.ToString() == "Open")
            {
                connection.Close();
                Console.WriteLine("Connection to SQL is closing...");
            }
        }
    }
}
