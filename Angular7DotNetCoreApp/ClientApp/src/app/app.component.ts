import { Component } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
//import { HttpClient } from 'selenium-webdriver/http';
import { Model } from './Model';
import { forEach } from '@angular/router/src/utils/collection';


//interface Course {
//  description: string;
//  courseListIcon: string;
//  iconUrl: string;
//  longDescription: string;
//  url: string;
//}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './freelancer.css']
})

  @Injectable({
    providedIn: 'root'
  })

export class AppComponent {
  title = 'Xou title';
  subtitle = "Xou subtitle";
  datetime = Date.now();
  private baseUri: string = 'https://localhost:5001/api';
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  public model: Model;
  tempArray: Object;
  public ArrayEmployees: Array<EmployeesClass> = [];
  sqldata = this.SQLReadAllRow();

  incrementValue: number[] = [];
  decrementValue: number[] = [];
  constructor(private http: HttpClient) {
    for (let index = 1; index <= 200; index++) {
      this.incrementValue.push(index);
    }

    for (let int1 = 400; int1 >= 201; int1--) {
      this.decrementValue.push(int1);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.decrementValue, event.previousIndex, event.currentIndex);
  }

  CreateButtonFunc() {
    var variable;
    let promise = new Promise((resolve, reject) => {
      let apiUrl = this.baseUri + '/values/test';
      console.log("Executing API call to URL: " + apiUrl);
      this.http.get(apiUrl)
        .toPromise()
        .then(
          response => { //success
            console.log(response); //response is automatically JSON format

            //can alert response locally but cannot assign to external variable
            alert(response[0]);
            alert(response[1]);
            resolve();
        });
    });
    return promise;
  }

  testAssign = "";
  //ArrayEmployees: Array<EmployeesClass>[]; 
  PromiseToEmployees(result) {
    //let ArrayEmployees: Array<EmployeesClass> = [];
    var ArrayEmployees: Array<EmployeesClass> = [];
    result.forEach(function (value) {
      var Employees: EmployeesClass = {
        EmployeeID: value.employeeID,
        LastName: value.lastName,
        FirstName: value.firstName,
        Address: value.address,
        City: value.city
      }
      ArrayEmployees.push(Employees);
    });

    return ArrayEmployees;
  }

  //test that the testAssign variable is actually publicly assigned 
  ReadTest() {
    alert(this.testAssign);
  }

  SQLRead() {
    //2. works withpromise
    let promise = new Promise((resolve, reject) => {
      let apiUrl = this.baseUri + '/SQL/Read';
      console.log("Executing API call to URL: " + apiUrl);
      this.http.get(apiUrl, { responseType: 'json' })
        .toPromise()
        .then(
          response => { //success
            console.log(response); //response is automatically JSON format

            //callback to assign to external variable while in promise
            this.PromiseToEmployees(response);
            resolve();
          });
    });
    return promise;
  }

  SQLReadAllRow() {

    let output = "";
    let promise = new Promise((resolve, reject) => {
      let apiUrl = this.baseUri + '/SQL/GetAllDataFromTable';
      console.log("Executing API call to URL: " + apiUrl);
      this.http.get(apiUrl, { responseType: 'json' })
        .toPromise()
        .then(
          response => { //success
            this.tempArray = response;
            this.ArrayEmployees = this.PromiseToEmployees(this.tempArray);
            console.log(this.ArrayEmployees[0].FirstName);
            resolve();
          });
    });
    return promise;
  }


}
